
import java.util.Scanner;

public class P12403 { // P 12403
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Integer nbCases = Integer.parseInt(sc.nextLine());
		Integer amount = 0;
		for(int i = 0; i < nbCases; i++) {
			String ligne = sc.nextLine();
			switch (ligne) {
			case "report":
				System.out.println(amount);
				break;
			default:
				String [] tab = ligne.split(" ");
				amount += Integer.parseInt(tab[1]);
				break;
			}
		}
		sc.close();
	}

}
