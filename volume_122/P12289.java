import java.util.Scanner;

public class P12289 { // P 12289

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Integer nbCases = Integer.parseInt(sc.nextLine());
		for(int i = 1; i <= nbCases; i++) {
			String ligne = sc.nextLine();
			if(ligne.length() == 5) {
				System.out.println("3");
			} else if((ligne.charAt(0) == 'o' && ligne.charAt(1) == 'n')
					|| (ligne.charAt(1) == 'n' && ligne.charAt(2) == 'e')
					|| (ligne.charAt(0) == 'o' && ligne.charAt(2) == 'e')) {
				System.out.println(1);
				
			} else {
				System.out.println(2);
			}
		}
		sc.close();
	}

}

