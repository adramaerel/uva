import java.util.Scanner;

public class Main { // P 11547

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String ligne;
		int i = 1;
		while(!((ligne = sc.nextLine()).equals("#"))) {
			switch (ligne) {
			case "HELLO":
				System.out.printf("Case %d: ENGLISH\n", i);
				break;
			case "HOLA":
				System.out.printf("Case %d: SPANISH\n", i);
				break;
			case "BONJOUR":
				System.out.printf("Case %d: FRENCH\n", i);
				break;
			case "CIAO":
				System.out.printf("Case %d: ITALIAN\n", i);
				break;
			case "ZDRAVSTVUJTE":
				System.out.printf("Case %d: RUSSIAN\n", i);
				break;
			case "HALLO":
				System.out.printf("Case %d: GERMAN\n", i);
				break;
			default:
				System.out.printf("Case %d: UNKNOWN\n", i);
				break;
			}
			i++	;
		}
	}

}

