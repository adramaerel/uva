
import java.util.Scanner;

public class P272 {
	
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		boolean isFirst = true;
		
		while(sc.hasNext()) {
			StringBuilder sb = new StringBuilder("");
			String ligne = sc.nextLine();
			for(int i = 0; i < ligne.length(); i++) {
				if(ligne.charAt(i) == '"') {
					if(isFirst) {
						sb.append("``");
						isFirst = !isFirst;
					} else {
						sb.append("\'\'");
						isFirst = !isFirst;
							
					}
				} else {
					sb.append(ligne.charAt(i));
				}
			}
			System.out.println(sb.toString());
		}
		
		sc.close();
	}

}

