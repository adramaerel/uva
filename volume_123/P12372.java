import java.util.Scanner;

public class P12372 { // P 12372

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Integer nbCases = Integer.parseInt(sc.nextLine());
		for(int i = 1; i <= nbCases; i++) {
			int a = sc.nextInt();
			int b = sc.nextInt();
			int c = sc.nextInt();
			if(a <= 20 && b <= 20 && c <= 20)
				System.out.printf("Case %d: good\n", i);
			else
				System.out.printf("Case %d: bad\n", i);
		}
		sc.close();
	}

}

