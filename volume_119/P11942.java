import java.util.Scanner;
 
public class P11942 {
 
               public static void main(String[] args) {
 
                              Scanner sc = new Scanner(System.in);
                              Integer nb = sc.nextInt();
                              sc.nextLine();
                              System.out.println("Lumberjacks:");
                              for(int i = 0 ; i < nb; i++) {
                                            String ligne = sc.nextLine();
                                            String [] tab = ligne.split(" ");
                                            boolean croissant = true;
                                            boolean decroissant = true;
                                            for(int j = 1; j < 10; j++) {
                                                           if(Integer.parseInt(tab[j]) > Integer.parseInt(tab[j - 1])) {
                                                                          decroissant = false;
                                                           }
                                                           if(Integer.parseInt(tab[j]) < Integer.parseInt(tab[j - 1])) {
                                                                          croissant = false;
                                                           }
                                            }
                                            System.out.println(croissant == false && decroissant == false ? "Unordered" : "Ordered");
                              }
               }
 
}
