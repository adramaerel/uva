import java.util.Scanner;

public class Main { // P 11875

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Integer nb = sc.nextInt();
		for(int i = 0; i < nb; ) {
			int j = sc.nextInt();
			for(int k = 0; k < j; k++) {
				if(k == j/2) {
					int moyenne = sc.nextInt();
					System.out.println("Case " + (++i) + ": " + moyenne);
				} else {
					sc.nextInt();
				}
			}
		}
	}

}

