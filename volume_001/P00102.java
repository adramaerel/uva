import java.util.Scanner;
 
public class P00102 {
 
               public static void main(String[] args) {
 
                             
                              Scanner sc = new Scanner(System.in);
                              while(sc.hasNext()) {
                                           
                                            Long max = Long.MAX_VALUE;
                                            String init = "";
                                           
                                            Long[] solutions = new Long[6];
                                            String[] name = new String[6];
                                           
                                            Long int1 = sc.nextLong(); // C bottles in bin 1
                                            Long int2 = sc.nextLong(); // G bottles in bin 1
                                            Long int3 = sc.nextLong(); // B bottles in bin 1
                                            Long int4 = sc.nextLong(); // C bottles in bin 2
                                            Long int5 = sc.nextLong(); // G bottles in bin 2
                                            Long int6 = sc.nextLong(); // B bottles in bin 2
                                            Long int7 = sc.nextLong(); // C bottles in bin 3
                                            Long int8 = sc.nextLong(); // G bottles in bin 3
                                            Long int9 = sc.nextLong(); // B bottles in bin 3
                                           
                                            // solution BCG
                                           
                                            solutions[0] = int2 + int3 + int4 + int5 + int7 + int9;
                                            name[0] = "BCG";
                                           
                                            // solution BGC
                                           
                                            solutions[1] = int2 + int3 + int4 + int6 + int7 + int8;
                                            name[1] = "BGC";
                                           
                                            // solution CBG
                                           
                                            solutions[2] = int1 + int2 + int5 + int6 + int7 + int9;
                                            name[2] = "CBG";
                                           
                                            // solution CGB
                                           
                                            solutions[3] = int1 + int2 + int4 + int6 + int8 + int9;
                                            name[3] = "CGB";
                                           
                                            // solution GBC
                                           
                                            solutions[4] = int1 + int3 + int5 + int6 + int7 + int8;
                                            name[4] = "GBC";
                                           
                                            // solution GCB
                                           
                                            solutions[5] = int1 + int3 + int4 + int5 + int8 + int9;
                                            name[5] = "GCB";
                                           
                                            for(int i = 0; i < 6; i++) {
                                                           if(solutions[i] < max) {
                                                                          max = solutions[i];
                                                                          init = name[i];
                                                           }
                                            }
                                            System.out.println(init + " " + max);
                                           
                                           
                                           
                              }
                              sc.close();
               }
 
}
