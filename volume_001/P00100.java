import java.util.Scanner;

public class Main {

	static int next(int n) {
		
		return n % 2 == 0 ? n / 2 :  3 * n +1;
	}
	
	static int cycle_length(int n) {
		int compteur = 1;
		while(n != 1) {
			compteur++;
			n = next(n);
		}
		return compteur;
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
				
		while(sc.hasNextInt()) {
			Integer int1 = sc.nextInt();
			Integer int2 = sc.nextInt();
			boolean switched = false;
			if(int2 < int1) {
				switched = true;
				int c = int2;
				int2 = int1;
				int1 = c;
			}
			int max = 0;
			for(int i = int1; i <= int2; i++) {
				int cycle = cycle_length(i);
				if(cycle > max) {
					max = cycle;
				}
			}
			System.out.println(switched  ? int2 + " " + int1 + " " + max : int1 + " " + int2 + " " + max);
			
		}
		sc.close();
	}

}

