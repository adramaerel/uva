import java.util.Scanner;

public class P11172 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Integer caseNum = Integer.parseInt(sc.nextLine());
		for(int i = 0; i < caseNum; i++) {
			String [] tab = sc.nextLine().split(" ");
			int a = Integer.parseInt(tab[0]);
			int b = Integer.parseInt(tab[1]);
			if(a < b) {
				System.out.println("<");
			} else if(a == b) {
				System.out.println("=");
			} else {
				System.out.println(">");
			}
		}
		sc.close();
		
	}

}

