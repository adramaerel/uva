import java.util.Scanner;
 
public class P11764 {
              
              
               public static void main(String[] args) {
                             
                              Scanner sc = new Scanner(System.in);
                              Integer nb = Integer.parseInt(sc.nextLine());
                              for(int i = 0; i < nb;) {
                                            sc.nextLine();
                                            String ligne = sc.nextLine();
                                            String [] integers = ligne.split(" ");
                                            int up =0, down = 0;
                                            for(int j = 1; j < integers.length; j++) {
                                                           if(Integer.parseInt(integers[j]) > Integer.parseInt(integers[j - 1])) {
                                                                          up++;
                                                           } else if (Integer.parseInt(integers[j]) < Integer.parseInt(integers[j - 1])) {
                                                                          down++;
                                                           }
                                                          
                                            }
                                            System.out.println("Case " + (++i) +": " + up + " " + down);
                              }
                              sc.close();
               }
 
}
