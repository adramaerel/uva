

import java.util.Scanner;

public class P11727 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		Integer nb = sc.nextInt();

		for(int i = 1; i < nb + 1; i++) {
			Integer a = sc.nextInt();
			Integer b = sc.nextInt();
			Integer c = sc.nextInt();

			if(a <= b && b<=c) {

				System.out.printf("Case %d: %d\n", i, b);
				continue;
			}

			if(a <= c && c<=b) {

				System.out.printf("Case %d: %d\n", i, c);
				continue;
			}

			if(b <= a && a<=c) {

				System.out.printf("Case %d: %d\n", i, a);
				continue;
			}

			if(b <= c && c<=a) {

				System.out.printf("Case %d: %d\n", i, c);
				continue;
			}
			if(c <= a && a<=b) {

				System.out.printf("Case %d: %d\n", i, a);
				continue;
			}
			if(c <= b && b<=a) {

				System.out.printf("Case %d: %d\n", i, b);
				continue;
			}
		}
		sc.close();

	}

}


