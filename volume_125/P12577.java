import java.util.Scanner;

public class P12577 { // P 12577
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String ligne;
		int compteur = 1;
		while(!(ligne =sc.nextLine()).equals("*")) {
			
			switch (ligne) {
			case "Hajj":
				System.out.printf("Case %d: Hajj-e-Akbar\n", compteur);
				break;
			case "Umrah":
				System.out.printf("Case %d: Hajj-e-Asghar\n", compteur);
				break;

			default:
				break;
			}
			compteur++;
		}
		sc.close();
	}

}


