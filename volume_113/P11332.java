
import java.util.Scanner;

public class P11332 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		while(true) {
			Integer i = sc.nextInt();
			if(i == 0) {
				break;
			} else {
				System.out.println(g(i));
			}
		}
		
		sc.close();
	
	}

	static int g(int n) {
		if(n< 10)
			return n;
		else {
			String nstring = n + "";
			int sum = 0;
			for(int i = 0; i < nstring.length(); i++) {
				sum += Integer.parseInt(nstring.charAt(i) + "");
			}
			return g(sum);
		}
	}
	
}

