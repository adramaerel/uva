

import java.util.Scanner;

public class P10018 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Integer nbcases = Integer.parseInt(sc.nextLine());
		for(int i =0; i < nbcases; i++) {
			Integer j = 1;
			String ligne = ((Long)Long.parseLong(sc.nextLine())).toString();
			String palindrome = calcul_palindrome(ligne);
			Long sum = Long.parseLong(ligne) + Long.parseLong(palindrome);
			while(!is_palindrome(sum)) {
				j++;
				String sum2 = calcul_palindrome(sum+"");
				sum = Long.parseLong(sum2) + sum;
			}
				System.out.printf("%d %d\n", j, sum);

		}
	}

	private static boolean is_palindrome(Long sum) {
		String ligne = sum +"";
		for(int i = 0; i < ligne.length(); i++) {
			if(ligne.charAt(i) != ligne.charAt(ligne.length() -1 -i)) {
				return false;
			}
		}
		return true;
	}

	private static String calcul_palindrome(String ligne) {
		String palindrome = "";
		for(int i = ligne.length() -1; i >= 0; i--) {
			palindrome += ligne.charAt(i);
		}
		return palindrome;
	}

}

