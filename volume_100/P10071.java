import java.util.Scanner;

public class P10071 {

	public static void main(String [] args) {
	
		Scanner sc = new Scanner(System.in);
		while(sc.hasNextLine()) {
			final String line = sc.nextLine();
			final Scanner sc2 = new Scanner(line);
			try {
				final Integer velocite = sc2.nextInt();
				final Integer time = sc2.nextInt();
				System.out.println(velocite * time * 2);
			} catch (Exception e) {
			}
		}
	}

}
