import java.util.Scanner;

public class P11547 { // P 11547

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int nbCases = sc.nextInt();
		for(int i = 0; i < nbCases; i++) {
			Integer input = sc.nextInt();
			input = (((input * 567)/9 + 7492)*235)/47 - 498;
			input %= 100;
			input /= 10;
			System.out.println(Math.abs(input));
			
		}
	}

}

